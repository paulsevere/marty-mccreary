#![allow(warnings)]
#![feature(proc_macro)]
#[macro_use]
extern crate stdweb;
use std::ops::{Div, Rem};
use stdweb::js_export;
use stdweb::traits::*;
use stdweb::unstable::TryInto;
use stdweb::web::event::{ClickEvent, MouseMoveEvent, MouseOutEvent, MouseOverEvent};
use stdweb::web::html_element::ImageElement;
use stdweb::web::{document, window, Date, Element, HtmlElement, Node};
use stdweb::Value;

macro_rules! enclose {
    ( ($( $x:ident ),*) $y:expr ) => {
        {
            $(let $x = $x.clone();)*
            $y
        }
    };
}

fn toggle_jesus(show: bool) {
    let jesus = document().query_selector_all(".jesus").unwrap();
    let t = Date::new().get_milliseconds();
    let which = t.rem(3);

    jesus.iter().enumerate().for_each(|(i, node)| {
        if ((which as usize) == i) {
            let el: Element = Node::try_into(node).unwrap();
            el.set_attribute(
                "style",
                format!(
                    "display:{}; position: fixed;",
                    match show {
                        true => "inline-block",
                        _ => "none",
                    }
                ).as_str(),
            ).unwrap();
            let mut t = Date::new().get_milliseconds() as u32;
            if (i == 2) {
                t = t * 200;
            }
            window().set_timeout(
                || {
                    toggle_jesus(false);
                },
                t.div(8),
            );
        }
    });
}

// #[js_export]
fn flash_jesus(_: String) {
    toggle_jesus(true);
    let t = Date::new().get_milliseconds() as u32;

    window().set_timeout(
        || {
            toggle_jesus(false);
        },
        t.div(8),
    );
}

fn main() {
    stdweb::initialize();

    let marty = document().query_selector_all(".trigger").unwrap();
    for node in marty {
        node.add_event_listener(enclose!((node) move |_: MouseMoveEvent| {
        toggle_jesus(true);
    }));
        node.add_event_listener(enclose!((node) move |_: MouseOutEvent| {
        toggle_jesus(false);
    }));
    }

    stdweb::event_loop();
}
